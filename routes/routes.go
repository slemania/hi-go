package routes

import (
	"hi_go/app/base"
	"hi_go/routes/api"
	"hi_go/routes/web"
	"os"
)

type RouteInterface interface {
	Do(router *base.Router)
}

func Init() {
	router := base.NewRouter()
	api.Init().Do(router)
	web.Init().Do(router)

	err := router.Run(":" + os.Getenv("APP_PORT"))
	if err != nil {
		return
	}
}
