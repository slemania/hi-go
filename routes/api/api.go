package api

import (
	"hi_go/app/base"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

type api struct{}

func (a api) Do(router *base.Router) {
	api := router.Group("/api")

	// Just for example
	api.GET("/hello_world", func(c *gin.Context) {
		c.JSON(http.StatusOK, struct {
			Status  bool   `json:"status"`
			Message string `json:"message"`
		}{
			Status:  true,
			Message: "api hello world! This app was running on " + os.Getenv("APP_PORT"),
		})
	})
}

func Init() api {
	return api{}
}
