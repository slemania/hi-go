package controllers

import (
	"hi_go/app/base"
	"hi_go/app/response"

	"github.com/gin-gonic/gin"
)

type ExampleController struct{ base.Controller }

func (controller ExampleController) Index(c *gin.Context) {
	req := controller.Request(c).Data()

	c.JSON(200, response.Api().Success("Loaded", gin.H{
		"message": req.GetParam("message"),
		"status":  req.GetParam("status"),
		"data":    req.GetParam("data"),
	}))
}
