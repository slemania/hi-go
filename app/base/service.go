package base

import "hi_go/app/response"

type ServiceInterface interface {
	Do() response.ServiceResponse
}
