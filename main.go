package main

import (
	"hi_go/routes"
	"os"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	println("App started")
	time.LoadLocation(os.Getenv("TIME_ZONE"))
	err := godotenv.Load()
	if err != nil {
		return
	}
	routes.Init()
}
